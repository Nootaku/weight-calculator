use std::io;

fn main() {
    let mut earth_weight = String::new();
    let mut chosen_planet = String::new();

    println!("Please enter your weight (Kg):");
    io::stdin().read_line(&mut earth_weight).unwrap();

    println!("On which planet, would you like to know your weight ?");
    println!("You can choose from: Mercury, Venus, Moon, Mars, Jupyter, Saturn, Uranus, Neptune and Pluto");
    io::stdin().read_line(&mut chosen_planet).unwrap();

    let weight: f32 = earth_weight.trim().parse().unwrap();
    let planet = chosen_planet.trim().to_lowercase();

    match planet.as_str() {
        "mercury" => {
            let calculated_weight = calculate_weight_on_mercury(weight);
            println!("Your weight on Mercury is: {} Kg", calculated_weight);
        },
        "venus" => {
            let calculated_weight = calculate_weight_on_venus(weight);
            println!("Your weight on Venus is: {} Kg", calculated_weight);
        },
        "moon" => {
            let calculated_weight = calculate_weight_on_moon(weight);
            println!("Your weight on the Moon is: {} Kg", calculated_weight);
        },
        "mars" => {
            let calculated_weight = calculate_weight_on_mars(weight);
            println!("Your weight on Mars is: {} Kg", calculated_weight);
        },
        "jupyter" => {
            let calculated_weight = calculate_weight_on_jupyter(weight);
            println!("Your weight on Jupyter is: {} Kg", calculated_weight);
        },
        "saturn" => {
            let calculated_weight = calculate_weight_on_saturn(weight);
            println!("Your weight on Saturn is: {} Kg", calculated_weight);
        },
        "uranus" => {
            let calculated_weight = calculate_weight_on_uranus(weight);
            println!("Your weight on Uranus is: {} Kg", calculated_weight);
        },
        "neptune" => {
            let calculated_weight = calculate_weight_on_neptune(weight);
            println!("Your weight on Neptune is: {} Kg", calculated_weight);
        },
        "pluto" => {
            let calculated_weight = calculate_weight_on_pluto(weight);
            println!("Your weight on Pluto is: {} Kg", calculated_weight);
        },
        _ => {
            println!("The chosen planet ({}) is invalid.", planet)
        }
    }
}


fn calculate_weight_on_mercury(weight: f32) -> f32 {
    (weight / 9.81) * 3.7
}

fn calculate_weight_on_venus(weight: f32) -> f32 {
    (weight / 9.81) * 8.87
}

fn calculate_weight_on_moon(weight: f32) -> f32 {
    (weight / 9.81) * 1.62
}

fn calculate_weight_on_mars(weight: f32) -> f32 {
    (weight / 9.81) * 3.711
}

fn calculate_weight_on_jupyter(weight: f32) -> f32 {
    (weight / 9.81) * 24.79
}

fn calculate_weight_on_saturn(weight: f32) -> f32 {
    (weight / 9.81) * 10.44
}

fn calculate_weight_on_uranus(weight: f32) -> f32 {
    (weight / 9.81) * 8.69
}

fn calculate_weight_on_neptune(weight: f32) -> f32 {
    (weight / 9.81) * 11.15
}

fn calculate_weight_on_pluto(weight: f32) -> f32 {
    (weight / 9.81) * 0.5886
}