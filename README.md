# Discovering Rust with a simple APP

[![Developer](https://img.shields.io/badge/Developed_by-Nootaku_(MWattez)-informational?style=for-the-badge)](https://gitlab.com/Nootaku)<br/>
![Version](https://img.shields.io/badge/Version-1.0.0-yellow?style=for-the-badge)

This project is a simple weight per planet convertor.<br/>
The purpose of the project is to learn the Rust language by practice.

## Objectives

1. Get familiar with the Rust syntax
2. Discover the Rust's basic data types
    - characters
    - integers
    - floats
    - booleans
3. Understand ownership
4. Get familiar with the `std` library

---

## Rust syntax

### The data types

#### 1. Integers

Integers can be of multiple types:
- unsigned (`u`) -> can only be positive
- signed (`i`) -> can be positive or negative
- `8`, `16`, `32`, `64`, `128`: number of bits (size of the integer)
- `usize`: architecture dependant (32 or 64 bits)
- `isize`: architecture dependant (32 or 64 bits)

#### 2. Floats

There are 2 types of floats:
- `f32`: 32 bits precision
- `f64`: 64 bits precision

#### 3. Booleans

`true` or `false` - one byte

#### 4. Char

Single unicode value (always 4 bytes)

### Functions

```rust
fn my_function(a: u8) -> bool {
	// do stuff
	// tail expression
	true
}
```

### Macros

A macro is a piece of code that calls more code.

The syntax is:
```rust
my_macro!(my_args);
```

The main difference between a function and a macro is that a function needs to declare all arguments and types that function receive. A macro, on the other hand can be called with a variable number of parameters. And with different types.

Obviously the macro definition is more complex than functions.

I would say the 2 most important macros are:
- `println!()`
- `dbg!()`

### Variables

A variable is instanciated with the `let` keyword.

By default a variable is immutable. We can make it mutable with `let mut`. Also we can force a data type by adding `: bool`, for example.

## Standard library

The documentation can be found at [doc.rustlang.org/std](https://doc.rust-lang.org/std/).


## Ownership

The concept of ownership is extremely important in Rust. It is subject to 3 rules:
1. Each value in Rust is owned by a variable
2. When the owner (variable) goes out of scope, the value will be deallocated from memory
3. A value can only have one owner at the time

In our code we call:
```rust
// we create a variable 'earth_weight' that owns a value on the Heap.
let mut earth_weight = String::new();
// String is a type of smart pointer (meaning the allocated space in the Heap will be deallocated when 'earth_weight' goes out of scope)

// Now we want to borrow the value without changing the owner
// This is different than moving the ownership
io::stdin()::read_line(&mut earth_weight);
```

> ! Important
>
> In a single scope we can have either
> - a single mutable reference OR
> - as many immutable references as we want

This prevents _data races_.

Just like in C, we use the `&` to indicate that we want to use a copy of a pointer. Without it we would move the ownership of the value to the function that calls it. This is the case with the `calculate_weight_on_planet` functions.

## String vs str

In Rust there is a difference between a `String` that is ... a string and a `str` that corresponds to a string slice.

When we create a string literal (`let my_var = "my wonderful string";`) this actually creates a `str`.

The biggest difference between the two is that a `str` cannot be modified whereas a `String` can be modified.

This is why we often see the following syntax:
- `let my_var = "my wonderful string".to_string();`
- `let my_var = String::from("my wonderful string");`

This makes a `String` from a `str`.

[Here is a great article](https://blog.logrocket.com/understanding-rust-string-str) explaining the difference.

Please note that, since `str` cannot be modified, it often requires borrowing (`&str`). This is especially the case in `struct` definitions.

### Differences in memory

A `str` is internally composed of:
- pointer to some bytes (=> to [my wonderful string])
- lenght (19)

A `String` is internally composed of:
- pointer to some bytes (=> to [my wonderful string])
- lenght (19)
- capacity (may be higher than what is being used)

This means that a `str` is only a part of a `String`.